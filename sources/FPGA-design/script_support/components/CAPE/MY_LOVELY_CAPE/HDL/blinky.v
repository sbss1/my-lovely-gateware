`timescale 1ns/100ps

module blinky(
    input    clk,
    input    resetn,
    output   blink
);

reg [26:0] counter; // 27-bit counter to accommodate large count values
reg state;

assign blink = state;

always @(posedge clk or negedge resetn) begin
    if (~resetn) begin
        counter <= 27'b0;
        state <= 1'b0;
    end else begin
        if (counter < 100000000 - 1) begin // 2 seconds on
            counter <= counter + 1;
            state <= 1'b1;
        end else if (counter < 125000000 - 1) begin // 0.5 seconds off
            counter <= counter + 1;
            state <= 1'b0;
        end else begin
            counter <= 27'b0; // Reset counter after complete cycle
        end
    end
end

endmodule
